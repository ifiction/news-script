<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek;

use Mediawiki\Api\Service\FileUploader;
use Mediawiki\Api\MediawikiApi;
use Mediawiki\Api\ApiUser;
use Mediawiki\Api\MediawikiFactory;
use Mediawiki\DataModel\Content;
use Mediawiki\DataModel\Title;
use Mediawiki\DataModel\PageIdentifier;
use Mediawiki\DataModel\Revision;
use Cocur\Slugify\Slugify;

class Wikipage {
  protected $game;
  protected $api;
  protected $services;
  protected $content;
  protected $fileUploader;
  protected $covername;

  public function __construct($game) {
    global $config;

    $this->game = $game;

    if (!$config['DRY_RUN']) {
      try {
        // Log in to a wiki
        $api = new MediawikiApi( $config['WIKI'] );
        $api->login( new ApiUser( $config['WIKIUSER'], $config['WIKIPASSWORD'] ) );
        $services = new MediawikiFactory( $api );
        $this->api = $api;
        $this->services = $services;
        $this->fileUploader = $services->newFileUploader();
      } catch (\Exception $e) {
        echo 'Ошибка соединения.'.PHP_EOL;
        echo $e->getMessage();
        echo $e->getTraceAsString();
        $config['DRY_RUN'] = true;
      }
    }
  }
  public function create() {
    global $config;
    $slugify = new Slugify();

    if (!empty($this->game->image)) {
      $filename = preg_replace('/\?.*/', '', basename($this->game->image));
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      $this->covername = $slugify->slugify($this->game->title, '_').'.'.$extension;
    }
    if (!empty($this->game->image_data)) {
      $extension = 'jpg';
      if (isset($this->game->image_extension)) {
        $extension = $this->game->image_extension;
      }
      $this->covername = $slugify->slugify($this->game->title, '_').'.'.$extension;
      $filename = $this->covername.'.'.$extension;
    }

    $pagetitle = strtr($this->game->title, [
      '|' => '-'
    ]);
    if (empty($pagetitle)) {
      echo 'ERROR: Page has no title.';
      print_r($this->game);
      return;
    }
    $exists = $this->exists($pagetitle);
    if (!$config['DRY_RUN'] && !$exists) {
      if (!empty($this->game->image)) {
        if ($this->services->newPageGetter()->getFromTitle($this->covername)) {
          $image = file_get_contents($this->game->image);
          file_put_contents($filename, $image);
          $extension = mime_content_type($filename);
          if (strpos($extension, 'image') !== FALSE) {
            $extension = str_replace('image/', '', $extension);
            $this->covername = $slugify->slugify($this->game->title, '_').'.'.$extension;
            $this->fileUploader->upload($this->covername, $filename);
          }
          unlink($filename);
        }
      }
      if (!empty($this->game->image_data)) {
        if ($this->services->newPageGetter()->getFromTitle($this->covername)) {
          $image = $this->game->image_data;
          file_put_contents($filename, $image);
          $this->fileUploader->upload($this->covername, $filename);
          unlink($filename);
        }
      }
    }

    $this->makeContent();

    if (!$config['DRY_RUN'] && !$exists) {
      $newContent = new Content( $this->content );
      $title = new Title($pagetitle);
      $identifier = new PageIdentifier($title);
      $revision = new Revision($newContent, $identifier);
      $this->services->newRevisionSaver()->save($revision);
      return true;
    } else {
      if ($exists) {
        echo "Страница игры уже существует. Автосоздание невозможно.\n";
        echo $this->content;
        return false;
      }
      if ($config['DRY_RUN']) {
        echo "Черновой режим. Автосоздание невозможно.\n";
      }
      echo $this->content;
      return true;
    }
  }
  protected function makeContent() {
    $this->content = '{{game info';

    $this->txtadd('title', ' |название='.$this->game->title);
    if (is_array($this->game->author) && count($this->game->author) === 1) {
      $this->game->author = trim($this->game->author[0]);
    }

    if (is_array($this->game->author) && count($this->game->author) > 0) {
      $this->content .= PHP_EOL.' |автор=';
      $i = 0;
      $l = count($this->game->author);
      foreach ($this->game->author as $author_name) {
        $this->content .= '[[Автор::'.$author_name.']]';
        $i++;
        if ($i < $l) {
          $this->content .= '; ';
        }
      }
    } else {
      $this->txtadd('author', ' |автор=[[Автор::'.$this->game->author.']]');
    }
    if (!empty($this->game->date)) {
      $date =$this->game->date->format('d.m.Y');
      $this->content .= PHP_EOL.' |вышла='.$date;
    }
    $this->txtadd('platform', ' |платформа='.$this->game->platform);
    $this->txtadd('image', ' |обложка='.$this->covername);
    $this->txtadd('image_data', ' |обложка='.$this->covername);
    $this->txtadd('language', ' |язык='.$this->game->language);
    if (is_array($this->game->themes) && !empty($this->game->themes)) {
      $this->content .= PHP_EOL.' |темы='.implode(',', $this->game->themes);
    } else if (!empty($this->game->themes)) {
      $this->content .= PHP_EOL.' |темы='.$this->game->themes;
    }

    $this->content .= "\n}}\n";

    $this->txtadd('description', $this->game->description);
    if (!empty($this->game->url_download) || !empty($this->game->url_online)) {
      $this->content .= "\n== Версии ==";
    }
    $this->txtadd('url_online', PHP_EOL.'* ['.$this->game->url_online.' '.$this->game->url_online_description.']');
    if (!empty($this->game->url_download)) {
      if (!empty($this->game->url_download_description)) {
        $this->content .= PHP_EOL.'* ['.$this->game->url_download.' '.$this->game->url_download_description.']';
      } else {
        $this->content .= PHP_EOL.'* ['.$this->game->url_download.' Скачать игру]';
      }
    }
    if (!empty($this->game->url_discussion) || !empty($this->game->url)) {
      $this->content .= "\n== Ссылки ==";
    }
    $this->txtadd('url_discussion', '* ['.$this->game->url_discussion.' Обсуждение игры]');
    $this->txtadd('url', '* ['.$this->game->url.' Страница игры]');
    if (!empty($this->game->categories) && is_array($this->game->categories)) {
      $this->content .= PHP_EOL;
      foreach ($this->game->categories as $category) {
        $this->content .= '[[Категория:'.$category.']]';
      }
    } else {
      if (!empty($this->game->categories)) {
        $this->content .= PHP_EOL.'[[Категория:'.$this->game->categories.']]';
      }
    }
  }

  protected function txtadd($param, $text) {
    if (!empty($this->game->$param)) {
      $this->content .= PHP_EOL.rtrim($text);
    }
  }

  /**
   * Checks if the page exists.
   *
   * @param string $pagename
   * @return boolean
   */
  protected function exists($pagename) {
    global $config;
    if ($config['DRY_RUN']) {
      return false;
    }
    $page = $this->services->newPageGetter()->getFromTitle((string) $pagename);
    return !($page->getId() === 0);
  }
}
