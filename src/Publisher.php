<?php
namespace Oreolek;

abstract class Publisher {
  protected $dry_run = true;

  public function __construct($config) {
    $this->dry_run = $config['DRY_RUN'];
  }

  abstract protected function _publish(string $text);

  public function publish(string $text): void {
    if (empty($this->client)) {
      return;
    }
    try {
      $this->_publish($text);
    } catch (\Exception $e) {
      echo $e;
      return;
    }
    if (!$this->dry_run) {
      file_put_contents('.lastrun', time());
    }
  }
}
