<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Instory extends Source {
  public $title = "Instory";
  protected function parse() {
    $this->parseFeed('http://instory.su/feed/');
  }
  protected function parseFeed($feedUrl) {
    $string = $this->get_text($feedUrl);
    $string = mb_convert_encoding($string, 'UTF-8', 'auto');
    $service = new \Sabre\Xml\Service();
    $service->elementMap = [
      '{}item' => function(\Sabre\Xml\Reader $reader) {
        return \Sabre\Xml\Deserializer\keyValue($reader, '');
      },
      '{}channel' => function(\Sabre\Xml\Reader $reader) {
        return \Sabre\Xml\Deserializer\repeatingElements($reader, '{}item');
      },
    ];
    try {
      $games = $service->parse($string)[0]['value'];
    } catch (\Sabre\Xml\LibXMLException $e) {
      echo $e->getMessage();
      echo $e->getTraceAsString();
      return "";
    }
    unset($string);
    foreach ($games as $gameBlock) {
      $date = strtotime($gameBlock['pubDate']);
      if ($date < $this->period) continue;
      $game = new Game;
      $game->title = trim($gameBlock['title']);
      $game->url = trim($gameBlock['link']);
      $game->description = trim($gameBlock['description']);
      $game->author = trim($gameBlock['{http://purl.org/dc/elements/1.1/}creator']);
      $this->output .= $game->print();
    }
  }
  public function checkPage($url) {
    return (strpos($url,'http://instory.top/') !== FALSE);
  }
  public function page($url) {
    $text = $this->get_text($url);
    $this->loadStr($text);
    unset($text);
    $game = new Game;
    $game->url = $url;
    $author = trim($this->dom->filter('.elementor-author-box__name')->first()->text());
    $date = $this->dom->filter('.elementor-icon-list-text.elementor-post-info__item.elementor-post-info__item--type-date')->first()->text();
    $date = str_replace('Дата:', '', $date);
    $date = str_replace(' в', '', $date);
    $date = new \DateTime($date);
    $title = $this->dom->filter('.elementor-heading-title.elementor-size-medium')->first();
    if ($title->count() > 0) {
      $game->title = htmlspecialchars_decode($title->text());
    }
    $game->image = $this->dom->filter('.elementor-image > a')->first()->getAttribute('href');
    $game->description = $this->dom->filter('.elementor-widget.elementor-widget-theme-post-content > div')->first()->text();
    $game->description = str_replace($this->dom->filter('.elementor-widget.elementor-widget-theme-post-content .rcl-rating-box')->first()->text(), '', $game->description);
    $game->description = trim($game->description);
    return $game;
  }

}
