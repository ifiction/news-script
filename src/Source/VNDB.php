<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class VNDB extends Source {
  public $title = "VNDB";
  protected function parse() {
    global $config;
    $client = new Client();
    $client->connect();
    $client->login($config['VNDB_USER'], $config['VNDB_PASSWORD']);
    $date = (new Date("1 week ago"))->format('Y-m-d');
    echo $date;
    die();
    $list = $client->sendCommand('get vn basic (released > "'.$date.'")');

    foreach ($list as $gameData) {
      $game = new Game;
      $game->title = $gameData->title;
      $game->author = $gameData->developer->display_name;
      $game->date = $gameData->published_on / 1000;
      $game->description = $descData->payload->metaDescription;
      $game->url = 'https://gamejolt.com/games/'.$gameData->slug.'/'.$gameData->id;
      $this->output .= $game->print();
    }
  }
}
