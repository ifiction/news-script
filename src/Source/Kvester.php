<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Kvester extends Source {
  public $title = "Квестер";
  protected $baseUrl = 'http://kvester.ru';
  protected function parse() {
    $text = $this->get_text($this->baseUrl."/catalog?sort=date");
    $this->loadStr($text);
    unset($text);
    $this->dom->filter('.catalog-item')->each(function($gameBlock){
      $date = trim($gameBlock->filter('.cell-2 .date')->text(), "() \t\n\r\0\x0B");
      $date = \DateTime::createFromFormat("d.m.y", $date);
      if ($date === false) return;
      $date = $date->format('U');
      if ($date < $this->period) return;
      $game = new Game;
      $game->author = trim(strip_tags($gameBlock->filter('.cell-2 .author')->text()));
      $game->title = trim($gameBlock->filter('.cell-2 h3 a')->text());
      $game->url = $this->baseUrl.trim($gameBlock->filter('.cell-2 h3 a')->attr('href'));
      $game->description = "";

      $this->loadStr($this->get_text($game->url));
      $game->description = $this->dom->filter('.description')->first()->text();

      $this->output .= $game->print();
    });
  }
  public function checkPage($url) {
    return (strpos($url, $this->baseUrl.'/game/') !== FALSE);
  }
  public function page($url) {
    $game = new Game;
    $game->url = $url;
    $game_id = (int) str_replace($this->baseUrl.'/game/', '', $url);
    $game->platform = 'Квестер';
    $game->title = trim($this->dom->filter(".qt-in-1 h2")->first()->text());
    $game->author = trim($this->dom->filter('.quest-info .author a')->text());
    $game->date = \DateTime::createFromFormat(
      'd.m.y',
      trim($this->dom->filter('.quest-info .date')->first()->text())
    );
    $game->url_online = $this->baseUrl.trim($this->dom->filter(".play a")->first()->attr('href'));
    $game->url_online_description = 'Играть онлайн на Квестер.ру';
    $game->description = trim($this->dom->filter('.quest-profile .description')->first()->text());
    $game->image = $this->baseUrl.trim($this->dom->filter('.quest-profile .cell-1 img')->first()->attr('src'));
    if ($game->author === 'Совместный') {
      $this->loadStr($this->get_text($this->baseUrl.'/game/quest/team/'.$game_id.'?ts_mode=public'));
      $game->author = [];
      $this->dom->filter('.team-item a .txt')->each(function($author) use($game){
        $game->author[] = $author->text();
      });
    }
    return $game;
  }
}
