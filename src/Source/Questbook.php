<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Questbook extends Source {
  public $title = "Сторигеймы";

  protected function parse() {
    global $argv;
    if (isset($argv[2])) {
      $game_page = $this->get_text($argv[2]);
      $this->loadStr($game_page, []);
      $this->output .= $this->page($argv[2])->print();
    } else {
      $this->parseFeed('https://quest-book.ru/directory/rss/');
      $this->parseFeed('https://quest-book.ru/online/rss.xml');
    }
  }

  protected function parseFeed($feedUrl) {
    $string = $this->get_text($feedUrl);
    $string = mb_convert_encoding($string, 'UTF-8', 'auto');
    $service = new \Sabre\Xml\Service();
    $service->elementMap = [
      '{}item' => function(\Sabre\Xml\Reader $reader) {
        return \Sabre\Xml\Deserializer\keyValue($reader, '');
      },
      '{}channel' => function(\Sabre\Xml\Reader $reader) {
        return \Sabre\Xml\Deserializer\repeatingElements($reader, '{}item');
      },
    ];
    try {
      $games = $service->parse($string)[0]['value'];
    } catch (\Sabre\Xml\LibXMLException $e) {
      echo $e->getMessage();
      echo $e->getTraceAsString();
      return "";
    }
    unset($string);
    foreach ($games as $gameBlock) {
      $date = strtotime($gameBlock['pubDate']);
      if ($date < $this->period) continue;
      $game = new Game;
      $game->title = trim($gameBlock['title'] ?? '');
      $game->url = trim($gameBlock['link'] ?? '');
      $game->url = str_replace('http://', 'https://', $game->url);
      $game->description = trim($gameBlock['description'] ?? '');
      $game->author = trim($gameBlock['author'] ?? '');
      $this->output .= $game->print();
    }
  }

  public function checkPage($url) {
    return (strpos($url,'https://quest-book.ru/') !== FALSE);
  }

  public function page($url) {
    $game = new Game;
    $game->url = $url;
    $title = $this->dom->filter('h2 a b');
    if ($title->count() > 0) {
      $title = $title->first()->text();
      $game->title = $title;
    } else {
      $title = $this->dom->filter('h2.mt-1');
      if ($title->count() > 0) {
        $title = $title->first()->text();
        $game->title = $title;
      }
    }
    $game->platform = 'Книга-игра';
    if (strpos($game->url, 'quest-book.ru/online/') !== FALSE) {
      $game->platform = 'Атрил';
    }
    $description = $this->dom->filter('div.col-md-8.col-sm-12 > div > div')->reduce(function($node) {
      if ($node->attr('style') === 'padding:5px;margin:2px;text-align:justify')
        return true;
      return false;
    });
    if ($description->count() > 0) {
      $description = $description->text();
      $game->description = $description;
    }
    $game->description = trim($game->description);
    $author = $this->dom->filter('div.col-md-8.col-sm-12 em span')->reduce(function($node) {
      if ($node->attr('itemprop') === 'author')
        return true;
      return false;
    });
    if ($author->count() > 0) {
      $author = $author->text();
      $game->author = $author;
    }
    return $game;
  }
}
