<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Hyperbook extends Source {
  public $title = "Гиперкнига";
  protected $games = array();
  protected $rootUrl = 'http://hyperbook.ru';
  protected function parse() {
    $text = $this->get_text($this->rootUrl.'/lib.php?sort=time');
    $this->loadStr($text);
    unset($text);
    $this->dom->filter("#listPubs h3 a")->each(function($link) {
      $game = new Game;
      $game->title = $link->text();
      $game->url = $link->attr('href');
      $game->url = str_replace('file', $this->rootUrl.'/comments.php?id=', $game->url);
      $this->games[] = $game;
    });
    $this->dom->filter("#listPubs div")->reduce(function($node) {
      if ($node->attr('style') === 'text-align:left;margin-bottom:4px;')
        return true;
      return false;
    })->each(function($author, $i) {
      $this->games[$i]->author = $author->text();
    });
    $this->dom->filter("#listPubs div")->reduce(function($node) {
      if ($node->attr('style') === 'float: left; width: 20%; text-align:right;')
        return true;
      return false;
    })->each(function($date, $i){
      $this->games[$i]->date = $date->text();
    });
    $this->dom->filter("#listPubs div")->reduce(function($node) {
      if ($node->attr('style') === NULL)
        return true;
      return false;
    })->each(function($dsc, $i){
      $this->games[$i]->description = $dsc->text();
    });
    foreach ($this->games as $game) {
      $date = \DateTime::createFromFormat('d.m.y', $game->date);
      if ($date === false) continue;
      $date = $date->format('U');
      if ($date < $this->period) continue;

      $this->output .= $game->print();
    }
  }
  public function checkPage($url) {
    return (strpos($url,$this->rootUrl.'/comments.php') !== FALSE);
  }
  public function page($url) {
    $game = new Game;
    $game->url = $url;
    $game->platform = 'AXMA Story Maker';
    $game->title = $this->dom->filter(".content h1")->first()->text();
    $game->title = trim(str_replace($this->dom->filter("h1 span")->first()->text(), '', $game->title));
    $game->author = $this->dom->filter(".content > div")->reduce(function($node) {
      if ($node->attr('style') === 'float: left; width: 50%; margin-bottom:14px; text-align: left;') {
        return true;
      }
      return false;
    })->first()->text();
    $game->date = $this->dom->filter(".content div.small")->reduce(function($node) {
      if ($node->attr('style') === 'float: left; width: 20%; text-align:right;')
        return true;
      return false;
    })->first()->text();
    $game->description = $this->dom->filter(".content div.small")->reduce(function($node) {
      if ($node->attr('style') === NULL)
        return true;
      return false;
    })->first()->text();
    $game->date = \DateTime::createFromFormat('d.m.y', $game->date);
    return $game;
  }
}
