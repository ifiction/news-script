<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

/**
 * Парсер для Apero.ru
 * Проблема парсера в том, что на Аперо часто поломана кодировка UTF-8.
 */
class Apero extends Source {
  public $title = "Apero";
  protected function parse() {
    $text = $this->get_text('http://apero.ru/Текстовые-игры/Песочница', [
      'order_by' => 'by_public',
    ]);
    $text = mb_convert_encoding($text, 'UTF-8', 'auto');
    $this->loadStr($text);
    $this->parseIndex();
    $text = $this->get_text('http://apero.ru/Текстовые-игры', [
      'order_by' => 'by_public',
    ]);
    $text = mb_convert_encoding($text, 'UTF-8', 'auto');
    $this->loadStr($text);
    $this->parseIndex();
  }
  protected function parseIndex()
  {
    $this->dom->filter('.tabled-game-block')->each(function($gameBlock){
      $formatter = new \IntlDateFormatter( 'ru', \IntlDateFormatter::LONG, \IntlDateFormatter::NONE );
      $date = trim($gameBlock->filter('.game-updated-block')->text(), "() \t\n\r\0\x0B");
      $date = str_replace('вчера', date('d.m.Y', strtotime('-1 day')), $date);
      $date = $formatter->parse($date);
      if ($date < $this->period) return;
      $game = new Game;
      $game->author = trim($gameBlock->filter('.game-author-block:first-child a')->text());
      $game->title = trim($gameBlock->filter('h2 a')->first()->text());
      $game->url = trim($gameBlock->filter('h2 a')->first()->attr('href'));
      $game->description = trim($gameBlock->filter('.game-desc-block')->first()->text());
      $this->output .= $game->print();
    });
  }
  public function checkPage($url) {
    return (strpos($url,'http://apero.ru/') !== FALSE);
  }
  public function page($url) {
    $game = new Game;
    $game->url = $url;
    $game->platform = 'Аперо';
    $game->title = $this->dom->filter('dd')->reduce(function($block) {
      if ($block->attr('itemprop') === 'name') {
        return true;
      }
      return false;
    })->text();
    $game->author = [];
    $this->dom->filter('dd a')->reduce(function($block){
      if ($block->attr('itemprop') === 'author') {
        return true;
      }
      return false;
    })->each(function($block) use($game){
      $game->author[] = $block->text();
    });
    $date = $this->dom->filter('meta')->reduce(function($block){
      if ($block->attr('itemprop') === 'datePublished') {
        return true;
      }
      return false;
    })->first();
    if ($date->count() > 0) {
      $date = $date->attr('content');
    } else {
      $date = NULL;
    }
    $game->date = \DateTime::createFromFormat('Y-M-d', $date);
    // TODO description
    return $game;
  }
}
