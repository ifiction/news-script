<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Axma extends Source {
  public $title = "Библиотека AXMA (самая новая)";
  protected $games = array();
  protected $rootUrl = 'https://axma.info/library/';
  protected function parse() {
    $i = 0;
    // Дата выхода последней игры на странице
    $lastDate = time();
    while ($lastDate >= $this->period) {
      $text = $this->get_text('https://axma.info/library/?sort=last&from='.$i);
      $this->loadStr($text);
      unset($text);
      $this->dom->filter("#listPubs h3 a")->each(function($link) {
        $game = new Game;
        $game->title = $link->text();
        $game->url = $link->attr('href');
        $game->url = str_replace('file', $this->rootUrl.'/comments.php?id=', $game->url);
        $this->games[] = $game;
      });
      // TODO парсер
      // TODO выставить время последней игры в lastDate
      $i += 5;
    }
  }
  public function checkPage($url) {
    return (strpos($url,$this->rootUrl) !== FALSE);
  }
}
