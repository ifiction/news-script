<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Gamejolt extends Source {
  public $title = "GameJolt";
  protected function parse_tag($url) {
    $data = json_decode($this->get_text($url));
    if (empty($data) or !isset($data->payload)) {
      echo 'GameJolt data empty';
      return;
    }
    $games = $data->payload->games;
    if (count($games) > 0) {
      foreach ($games as $gameData) {
        $descUrl = 'https://gamejolt.com/site-api/web/discover/games/overview/'.$gameData->id;
        $descData = json_decode($this->get_text($descUrl));
        $game = new Game;
        $game->title = $gameData->title;
        $game->author = $gameData->developer->display_name;
        $game->date = $gameData->published_on / 1000;
        $game->description = $descData->payload->metaDescription;
        $game->url = 'https://gamejolt.com/games/'.$gameData->slug.'/'.$gameData->id;
        if ($game->date < $this->period) {
          continue;
        }
        $this->output .= $game->print();
      }
    }
  }
  protected function parse() {
    try {
      $this->parse_tag("https://gamejolt.com/site-api/web/library/games/tag/twine");
      $this->parse_tag("https://gamejolt.com/site-api/web/library/games/tag/renpy");
      $this->parse_tag("https://gamejolt.com/site-api/web/library/games/tag/text");
      $this->parse_tag("https://gamejolt.com/site-api/web/library/games/tag/ascii");
    } catch (\Exception $e) {}
  }
}
