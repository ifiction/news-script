<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Anivisual extends Source {
  public $title = "Anivisual";
  protected $months = [
    'Января' => 'January',
    'Февраля' => 'February',
    'Марта' => 'March',
    'Апреля' => 'April',
    'Мая' => 'May',
    'Июня' => 'June',
    'Июля' => 'July',
    'Августа' => 'August',
    'Сентября' => 'September',
    'Октября' => 'October',
    'Ноября' => 'November',
    'Декабря' => 'December',
  ];
  protected function parse() {
    $text = $this->get_text('http://anivisual.net/stuff/1');
    $this->loadStr($text);
    unset($text);
    $this->dom->filter('.entryBlock')->each(function($gameBlock) {
      $date = trim($gameBlock->filter('.icon-calendar')->text());
      foreach ($this->months as $ruM => $enM) {
        $date = str_replace($ruM, $enM, $date);
      }
      $date = \DateTime::createFromFormat('d F Y', $date);
      if ($date !== false) {
        $date = $date->format('U');
        if ($date < $this->period) return;
      }
      $link = $gameBlock->filter('.novel-ttl a')->first();
      $link = 'http://anivisual.net'.$link->attr('href');
      $game = $this->page($link);
      //$game = new Game;
      //$game->title = htmlspecialchars_decode($link->html());
      //$game->url = 'http://anivisual.net'.$link->attr('href');
      //$game->description = $gameBlock->filter('span')->first()->text();
      
      $games[] = $game;

      $this->output .= $game->print();
    });
  }

  public function checkPage($url) {
    return (strpos($url,'://anivisual.net/stuff/') !== FALSE);
  }

  public function page($url) {
    $text = $this->get_text($url);
    $this->loadStr($text);
    unset($text);
    $game = new Game;
    $game->url = $url;
    $gameBlock = $this->dom->filter('#casing-box');
    $dateBlock = $this->dom->filter('.icon-calendar');
    $date = '';
    if ($dateBlock->count() > 0) {
      $date = trim($dateBlock->first()->text());
    }
    if (!empty($date)) {
      foreach ($this->months as $ruM => $enM) {
        $date = str_replace($ruM, $enM, $date);
      }
      $game->date = \DateTime::createFromFormat('d F Y', $date);
      unset($date);
    }
    $title = $this->dom->filter('h1.logo')->first();
    if ($title->count() > 0) {
      $game->title = trim(htmlspecialchars_decode($title->text()));
    }
    $game->description = $this->dom->filter('#content > section > span')->first()->text();
    $game->description = str_replace('(adsbygoogle = window.adsbygoogle || []).push({});', '', $game->description);
    $game->description = str_replace('Доп. ссылки: Доступно только для пользователей', '', $game->description);
    $game->description = trim($game->description);

    $sidebar = $gameBlock->filter('#sidebar')->first()->html();
    $pos_start = mb_strpos($sidebar, '<b>Автор:');
    $sidebar_search = trim(mb_substr($sidebar, $pos_start));
    $pos_end = mb_strpos($sidebar_search, '<br>');
    $sidebar_search = trim(mb_substr($sidebar_search, 0, $pos_end));
    $sidebar_search = str_replace('<b>Автор:</b>', '', $sidebar_search);
    $game->author = trim(strip_tags($sidebar_search));

    $pos_start = mb_strpos($sidebar, '<b>Перевод:');
    $sidebar_search = trim(mb_substr($sidebar, $pos_start));
    $pos_end = mb_strpos($sidebar_search, '<br>');
    $sidebar_search = trim(mb_substr($sidebar_search, 0, $pos_end));
    $sidebar_search = trim(strip_tags(str_replace('<b>Перевод:</b>', '', $sidebar_search)));
    if ($sidebar_search !== '') {
      $game->author .= ', пер. '.$sidebar_search;
    }
    return $game;
  }
}
