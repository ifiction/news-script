<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;
use \Symfony\Component\DomCrawler\Crawler;

class Textadventures extends Source {
  public $title = "Textadventures.co.uk";
  protected function parse() {
    $text = $this->get_text('http://textadventures.co.uk/games/latest');
    $this->loadStr($text);
    unset($text);
    $this->dom->filter('.games-item')->each(function($gameBlock){
      $game = new Game;
      $game->url = 'http://textadventures.co.uk'.$gameBlock->filter('.games-title a')->attr('href');
      $game->title = $gameBlock->filter('.games-title a')->text();
      $date = strtotime($gameBlock->filter('.games-date')->text());
      if ($date < $this->period) return;
      $text = $this->get_text($game->url);
      $game_page = new Crawler($text);
      unset($text);
      $game->author = str_replace('by ', '', $game_page->filter('h1 small')->text());
      $image = $game_page->filter('img.cover-image')->first();
      if ($image->count()) {
        $game->image = $image->attr('src');
      }
      $game_page = $game_page->filter('.col-md-12 .col-md-9 > .col-md-12')->first();
      if (!$game_page->count()) {
        $game_page = $game_page->filter('.col-md-12 .col-md-9 > .col-md-7')->first();
      }
      if ($game_page->count()) {
        $game->desc = trim($game_page->children()->first()->text());
      }

      $this->output .= $game->print();
    });
  }
}
