<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Qsp extends Source {
  public $title = "Библиотека QSP";
  protected function parse() {
    $text = $this->get_text("http://qsp.su/index.php?option=com_sobi2&sobi2Task=rss&no_html=1&catid=1&Itemid=55");
    $this->loadStr($text);
    unset($text);
    $this->dom->filter('channel item')->each(function($gameBlock){
      $date = trim($gameBlock->filter('pubDate')->text(), "() \t\n\r\0\x0B");
      $date = new \DateTime($date);
      if ($date === false) return;
      $date = $date->format('U');
      if ($date < $this->period) return;
      $game = new Game;
      $game->author = trim($gameBlock->filter('category')->text());
      $game->title = trim($gameBlock->filter('title')->text());
      $game->url = trim($gameBlock->filter('link:first-child')->text());
      $game->description = trim($gameBlock->filter('description')->text());
      $this->output .= $game->print();
    });
  }
  public function checkPage($url) {
    return (strpos($url,'http://qsp.su') !== FALSE);
  }
  public function page($url) {
    $game = new Game;
    $game->url = $url;
    try {
      $game->author = trim($this->dom->filter('#sobi2Details_field_author')->text());
      $game->author = trim(str_replace($this->dom->filter('#sobi2Listing_field_author_label')->text(), '', $game->author));
      $game->title = trim($this->dom->filter('.sobi2Details h1')->first()->text());
      $game->description = trim($this->dom->filter('#sobi2Details_field_description')->text());
      $game->platform = 'QSP';
      $game->url_download = trim($this->dom->filter('h2 a')->attr('href'));
      $game->url_download_description = 'Архив для интерпретатора QSP';
      $game->image = trim($this->dom->filter('.sobi2DetailsImage')->first()->attr('src'));
      preg_match('/\d?\d\.\d?\d\.\d{4}/', $this->dom->filter('.sobi2DetailsFooter tr:first-child td')->text(), $matches);
      $game->date = new \DateTime($matches[0]);
    } catch (\Exception $e) {
      echo 'Ошибка парсинга. Проверьте URL.'.PHP_EOL;
      echo $e->getTraceAsString();
      die();
    }
    return $game;
  }
}
