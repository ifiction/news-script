<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Urq extends Source {
  public $title = "Библиотека URQ";
  protected function parse() {
    $text = $this->get_text('http://urq.plut.info/node/209');
    $this->loadStr($text);
    unset($text);
    $games = $this->dom->filter('.view-NewGames tr')->each(function($gameBlock) {
      $game = new Game;
      $game->author = trim($gameBlock->filter('.views-field-taxonomy-vocabulary-2')->first()->text());
      $game->title = trim($gameBlock->filter('.views-field-title')->text());
      $game->url = 'http://urq.plut.info'.trim($gameBlock->filter('.views-field-title a')->attr('href'));
      $this->output .= $game->print();
    });
  }
  public function checkPage($url) {
    return (strpos($url,'http://urq.plut.info/node/') !== FALSE);
  }
  public function page($url) {
    $game = new Game;
    $game->url = $url;
    $gameBlock = $this->dom->filter('.content');
    $game->author = trim($gameBlock->filter('.field-name-taxonomy-vocabulary-2 a')->first()->text());
    $game->title = trim($this->dom->filter('h1.title')->first()->text());
    $game->description = $gameBlock->filter('.field-type-text-with-summary .field-items span.field-item')->first()->text();
    $game->platform = $gameBlock->filter('.field-name-taxonomy-vocabulary-5 .field-items span.field-item a')->first()->text();
    return $game;
  }
}
