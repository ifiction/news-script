<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;

class Instead extends Source {
  public $title = "INSTEAD репозиторий";
  protected $baseUrl = 'http://instead-games.ru/';
  protected function insteadfeed($url) {
    $text = $this->get_text($url);
    $this->loadStr($text);
    unset($text);
    $this->dom->filter('.game')->each(function($gameBlock) {
      $date = trim($gameBlock->filter('.b .date b')->text());
      $date = \DateTime::createFromFormat('Y.m.d', $date);
      $date = $date->format('U');
      if ($date < $this->period) return;
      $url = $this->baseUrl.trim($gameBlock->filter('h2:first-child a:first-child')->first()->attr('href'));
      $game = $this->page($url);
      $this->output .= $game->print();
    });
  }
  protected function parse() {
    $this->insteadfeed("http://instead-games.ru/");
    if (FORMAT === 'HTML') {
      $this->output .= "<h5>Песочница</h5>\n";
    } elseif (FORMAT === 'MARKDOWN') {
      $this->output .= "##### Песочница\n";
    }
    $this->insteadfeed("http://instead-games.ru/index.php?approved=0");
  }
  public function checkPage($url) {
    return (strpos($url,'http://instead-games.ru/game.php') !== FALSE);
  }
  public function page($url) {
    $text = $this->get_text($url);
    $this->loadStr($text);
    unset($text);
    $game = new Game;
    $game->url = $url;
    $game->platform = 'INSTEAD';
    $text = trim($this->dom->filter('#panel')->text());
    preg_match('/Дата: ([0-9]{4}\.[01][0-9]\.[0-3][0-9])Размер/', $text, $matches);
    $game->date = \DateTime::createFromFormat('Y.m.d', $matches[1]);
    preg_match('/Автор: (.+)Дата/', $text, $matches);
    $game->author = trim($matches[1]);
    $game->title = trim($this->dom->filter('h2')->first()->text());
    $game->description = trim($this->dom->filter('.gamedsc')->first()->html());
    if($this->dom->filter('#instead-em')->first()) {
      $game->url_online = $this->baseUrl.$this->dom->filter('#instead-em')->attr('href');
      $game->url_online_description = 'Играть онлайн';
    }
    $game->url_download = $this->baseUrl.ltrim($this->dom->selectLink('Скачать')->first()->attr('href'), '/');
    $link = $this->dom->selectLink('Обсудить')->first();
    if ($link->count() > 0) {
      $game->url_discussion = $link->attr('href');
    }
    $image = $this->dom->filter('#screenshots a')->first();
    if ($image->count() > 0) {
      $game->image = $this->baseUrl.$image->attr('href');
    }
    return $game;
  }
}
