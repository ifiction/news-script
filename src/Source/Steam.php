<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek\Source;

use \Oreolek\Game;
use \Oreolek\Source;
use \Symfony\Component\DomCrawler\Crawler;
use \GuzzleHttp\Cookie\CookieJar;
use \GuzzleHttp\Cookie\SetCookie;

class Steam extends Source {
  public $title = "Steam";
  protected $games = [];
  public $delayedLoad = true;
  protected $months = [
    'янв.' => 'January',
    'фев.' => 'February',
    'мар.' => 'March',
    'апр.' => 'April',
    'мая.' => 'May',
    'июн.' => 'June',
    'июл.' => 'July',
    'авг.' => 'August',
    'сен.' => 'September',
    'окт.' => 'October',
    'ноя.' => 'November',
    'дек.' => 'December',
  ];
  protected function parse_tag($tag) {
    $url = 'https://store.steampowered.com/search/';
    $url .= '?'.http_build_query([
      'sort_by' => 'Released_DESC',
      'term' => $tag,
      'displayterm' => $tag,
      'category1' => 998, // only games
    ]);
    $text = $this->get_text($url);
    $this->loadStr($text);
    unset($text);
    $this->dom->filter('#search_result_container a.search_result_row')->each(function($gameLink){
      $url = $gameLink->attr('href');
      $url = substr($url,0,strpos($url, '?')); // remove query string
      $game = $this->page($url);
      if ($game) {
        if ($game->date) {
          $date = $game->date->format('U');
          if ($date < $this->period) return;
        }
        $this->games[] = $game->print();
      }
    });
  }
  protected function parse() {
    global $argv;
    if (isset($argv[2])) {
      $game = $this->page($argv[2]);
      if (!$game) {
        return;
      }
      $this->output .= $game->print();
    } else {
      $this->parse_tag("text-based");
      $this->parse_tag("interactive fiction");
      $this->parse_tag("visual novel");
      $this->games = array_unique($this->games);
      foreach ($this->games as $game) {
        $this->output .= $game;
      }
    }
  }
  public function checkPage($url) {
    return (strpos($url,'store.steampowered.com/') !== FALSE);
  }
  public function page($url) {
    $this->cookies = new CookieJar(true);
    $this->cookies->setCookie(new SetCookie([
      'Domain' => 'store.steampowered.com',
      'Name' => 'mature_content',
      'Value' => 1
    ]));
    $this->cookies->setCookie(new SetCookie([
      'Domain' => 'store.steampowered.com',
      'Name' => 'Steam_Language',
      'Value' => 'russian'
    ]));
    $this->cookies->setCookie(new SetCookie([
      'Domain' => 'store.steampowered.com',
      'Name' => 'timezoneOffset',
      'Value' => '0,0'
    ]));
    $this->loadStr($this->get_text($url));

    $game = new Game;
    $game->url = $url;

    $comingsoon = $this->dom->filter('div.game_area_comingsoon')->first();
    if ($comingsoon->count() > 0) {
      // we are skipping preorders and coming soon games
      return false;
    }
    $title = $this->dom->filter('div.apphub_AppName')->first();
    if ($title->count() > 0) {
      $game->title = trim($title->text());
    }
    $dsc = $this->dom->filter('div.game_description_snippet')->first();
    if ($dsc->count() > 0) {
      $game->description = trim($dsc->text());
    }
    $author = $this->dom->filter('div#developers_list')->first();
    if ($author->count() > 0) {
      $game->author = trim($author->text());
      if (strpos($game->author, ',') !== FALSE) {
        $game->author = explode(',', $game->author);
        $game->author = array_map('trim', $game->author);
      }
    }
    $image = $this->dom->filter('img.game_header_image_full')->first();
    if ($image->count() > 0) {
      $game->image = $image->attr('src');
    }
    $game->categories = 'Коммерческая ИЛ';
    $languages = $this->dom->filter('.game_language_options tr td:first-child');
    if ($languages->count() > 0) {
      $game->language = [];
      foreach ($languages as $language) {
        $game->language[] = trim($language->nodeValue);
      }
      $game->language = implode(', ', $game->language);
    }
    $date = $this->dom->filter('div.date')->first();
    if ($date->count() > 0) {
      $date = $date->text();
      $game->date = \DateTime::createFromFormat('d M, Y', $date);
      if ($game->date === FALSE) { // если Steam отдал страницу на русском
        foreach ($this->months as $ruM => $enM) {
          $date = str_replace($ruM, $enM, $date);
        }
        $game->date = \DateTime::createFromFormat('d F Y', $date);
      }
    }
    return $game;
  }
}
