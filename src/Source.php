<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Oreolek;

use \Symfony\Component\DomCrawler\Crawler;
use \GuzzleHttp\Client as GuzzleClient;
use Game;

abstract class Source {
  // Title
  public $title;
  // Optional warning or note
  public $warning = FALSE;
  protected $dom;
  protected $period;
  protected $output;
  protected $cookies = '';
  public function __construct() {
    $this->period = strtotime("1 week ago");
    $this->output = '';
  }
  /**
   * Should be load the page before the parsing or during
   *
   * @var boolean
   */
  public $delayedLoad = false;

  public function loadStr($html) {
    $this->dom = new Crawler($html);
  }

  /**
   * Function to start the section.
   * @param whether to return or print the text
   *
   * @return string|void
   */
  protected function startSection($return = false) {
    if (FORMAT === 'MARKDOWN') {
      $text = "\n#### ".$this->title."\n";
    }
    if (FORMAT === 'HTML') {
      $text = "\n<spoiler title='".$this->title."'><h4>".$this->title."</h4>";
      if (!empty($this->warning)) {
        $text .= '<small>'.$this->warning.'</small>';
      }
      $text .= "<ul>\n";
    }
    if ($return) {
      return $text;
    }
    $this->output .= $text;
  }

  protected function endSection() {
    if (FORMAT === 'HTML') {
      $text = "</ul></spoiler>\n";
    }
    $this->output .= $text;
  }

  abstract protected function parse();

  /**
   * System function to download page HTML.
   *
   * @return string
   */
  public function get_text($url, $post = []) {
    $client = new GuzzleClient([
      'timeout' => 30,
    ]);
    if ($post === []) {
      $response = $client->request('GET', $url, [
        'cookies' => $this->cookies,
      ]);
    } else {
      $response = $client->request('POST', $url, [
        'form_params' => $post,
        'cookies' => $this->cookies,
      ]);
    }
    return (string) $response->getBody();
  }

  /**
   * GET JSON data.
   */
  public function get_json($url) {
    $client = new GuzzleClient([
      'timeout' => 30,
    ]);
    $response = $client->request('GET', $url, [
      'cookies' => $this->cookies,
    ]);
    $text = (string) $response->getBody();
    return json_decode($text);
  }

  /**
   * Print the game line and catch all exceptions.
   *
   * @return void
   */
  public function check() {
    try {
      $this->startSection();
      try {
        $this->parse();
      } catch (\Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
        echo $e->getTraceAsString();
        return;
      }
      $this->endSection();
      if ($this->output === $this->startSection(true)) // nothing to print
        return;
      echo $this->output;
    } catch (Exception $e) {
      echo $e->getMessage();
      echo $e->getTraceAsString();
    }
  }

  /**
   * Check if URL corresponds to this source.
   *
   * @return boolean
   */
  function checkPage($url) {
    return false;
  }
}
