<?php
namespace Oreolek\Publisher;
use \Oreolek\Publisher;

class Telegram extends Publisher {
  protected $client;
  protected $chat_id;
  public function __construct($config) {
    Publisher::__construct($config);
    if ($config['TELEGRAM'] === true) {
      $this->chat_id = $config['TELEGRAM_CHAT_ID'];
      $this->client = new Longman\TelegramBot\Telegram(
        $config['TELEGRAM_API_KEY'],
        $config['TELEGRAM_BOT_NAME']
      );
    }
  }

  public function _publish(string $text): void {
    \Longman\TelegramBot\Request::sendMessage([
      'chat_id' => $this->chat_id,
      'text' => $text,
      'parse_mode' => 'Markdown'
    ]);
  }
}
