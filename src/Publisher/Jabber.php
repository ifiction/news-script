<?php
namespace Oreolek\Publisher;
use \Oreolek\Publisher;
use \Norgul\Xmpp\Options;
use \Norgul\Xmpp\XmppClient;

class Jabber extends Publisher {
  protected $client;

  public function __construct($config) {
    Publisher::__construct($config);
    if ($config['JABBER'] === true) {
      $options = new Options();
      $options->setHost($config['JABBER_HOST']);
      $options->setUsername($config['JABBER_JID']);
      $options->setPassword($config['JABBER_PASSWORD']);
      $this->client = new XmppClient($options);
      $this->client->connect();
    }
  }

  public function __destruct() {
    $this->client->disconnect();
  }

  protected function _publish(string $text) {
    if (empty($this->text)) {
      return;
    }

    $xml = <<<END
<iq type='set'
    from='dicebot@oreolek.ru/blogbot'
    to='pubsub.oreolek.ru'
    id='publish1'>
  <pubsub xmlns='http://jabber.org/protocol/pubsub'>
    <publish node='princely_musings'>
      <item id='bnd81g37d61f49fgn581'>
        <entry xmlns='http://www.w3.org/2005/Atom'>
          <title>Soliloquy</title>
          <summary>
To be, or not to be: that is the question:
Whether 'tis nobler in the mind to suffer
The slings and arrows of outrageous fortune,
Or to take arms against a sea of troubles,
And by opposing end them?
          </summary>
          <link rel='alternate' type='text/html'
                href='http://denmark.lit/2003/12/13/atom03'/>
          <id>tag:denmark.lit,2003:entry-32397</id>
          <published>2003-12-13T18:30:02Z</published>
          <updated>2003-12-13T18:30:02Z</updated>
        </entry>
      </item>
    </publish>
  </pubsub>
</iq>
END;
    $this->client->send($xml);
    $response = $this->client->getResponse();
    $this->client->prettyPrint($response);
  }
}
