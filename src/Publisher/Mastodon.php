<?php
namespace Oreolek\Publisher;
use \Oreolek\Publisher;

class Mastodon extends Publisher {
  protected $client;

  public function __construct($config) {
    Publisher::__construct($config);
    if ($config['MASTODON'] === true) {
      $guzzle = new GuzzleClient([
        'timeout' => 30,
      ]);
      $this->client = new MastodonClient($guzzle);
      $this->client->domain($config['MASTODON_SERVER'])->token($config['MASTODON_ACCESS_TOKEN']);
    }
  }

  protected function _publish(string $text): void {
    $this->client->createStatus($text, [
      'language' => 'en'
    ]);
  }
}
