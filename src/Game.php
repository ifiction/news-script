<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Oreolek;

use \Pandoc\Pandoc;

class Game {
  public $url;
  public $title;
  public $author;
  /**
   * Полное или единственное описание.
   *
   * @var string
   */
  public $description;
  /**
   * Короткое описание.
   *
   * @var string
   */
  public $short_description;
  /**
   * Дата выхода игры.
   *
   * @var DateTime
   */
  public $date;
  /**
   * Path or URL to game cover.
   *
   * @var string
   */
  public $image;
  /**
   * Binary image data, game cover.
   * Most likely product of `file_get_contents`
   *
   * @var string
   */
  public $image_data;
  /**
   * Cover data extension (jpg, png)
   *
   * @var string
   */
  public $image_extension = 'jpg';
  public $platform;
  public $url_online;
  public $url_download;
  public $url_discussion;
  public $url_download_description;
  public $url_online_description;
  public $language;
  /**
   * Темы на вики.
   *
   * @var string[]
   */
  public $themes;
  /**
   * Категории на вики.
   *
   * @var string[]
   */
  public $categories;
  public function print() {
    $converter = new Pandoc();
    if (STYLE === 'RUS') {
      if (FORMAT === 'MARKDOWN') {
        $output = "* [«".trim($this->title)."»](".trim($this->url).")";
        if ($this->author) {
          $output .= " — *".trim($this->author)."*";
        }
      }
      if (FORMAT === 'HTML') {
        $output = '<li>';
        if ($this->author) {
          if (is_array($this->author)) {
            $output .= "<em>".implode(', ', array_map('trim', $this->author))."</em>";
          } else {
            $output .= "<em>".trim($this->author)."</em>";
          }
          $output .= ' — ';
        }
        $output .= "<a rel='nofollow' target='_blank' href='".trim($this->url)."'>«".trim($this->title)."»</a>";
      }
    }
    if (STYLE === 'ENG') {
      $output = "* [“".trim($this->title)."”](".trim($this->url).")";
      if ($this->author) {
        $output .= " by *".trim($this->author)."*";
      }
    }
    $description = trim($this->getDescription());
    if ($description !== '') {
      if (FORMAT === 'MARKDOWN') {
        $output .= "\n\n    > ".$converter->convert($this->getDescription(), 'html', 'markdown_github')."\n";
      }
      if (FORMAT === 'HTML') {
        $output .= "\n<blockquote>".$description."</blockquote>\n";
      }
      if (FORMAT === 'MARKDOWN') {
        $output .= "\n";
      }
    }
    if (FORMAT === 'HTML') {
      $output .= "</li>\n";
    }
    return $output;
  }

  /**
   * Serialization for array_unique function.
   */
  public function __toString() {
    if (!empty($this->url)) {
      return $this->url;
    } else {
      return $this->title;
    }
  }

  public function getDescription() {
    if ($this->short_description) {
      return $this->short_description;
    }
    return $this->description;
  }
}
