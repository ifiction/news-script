# Утилиты для отслеживания релизов текстовых игр
Изначально парсер для русской интерактивной литературы, теперь нечто большее.

Каждый скрипт делает что-то своё, но основной код у них общий.

### run.php
Этот был первым, он самый простой.
Он сканирует хостинги игр, находит новые релизы за последнюю неделю и выводит
в консоль список в форматах Markdown или HTML. Всё автоматически.

Если передать параметр, например, `itch`, то он сканирует только указанный сайт.

Второй параметр - URL страницы, чтобы просканировать страницу одной игры.

### bot.php
То же самое, но постоянно и постит вывод в Mastodon и/или Telegram.

### wiki.php
Сканирует страницу одной игры и создаёт записи на [IFWiki.](http://ifwiki.ru)

Если страница уже есть на вики, выводит текст, который хотел записать.
Автоматически заменять текст не пытается, это надо смотреть вручную.
Если находит обложку игры, тоже заливает её на вики.

### kril.php
Оформление результатов [КРИЛа 2018.](http://kril.ifiction.ru)

## Установка

1. Скопировать `config.yml.example` в `config.yml`, отредактировать.
    1. Параметр `DRY_RUN` отвечает за тест: `true` означает, что скрипты бота и вики не попытаются создавать страницы и постить записи, а выведут текст и закончат работу.
1. Установить `pandoc`
1. Скачать [`composer`](https://getcomposer.org/) и выполнить `composer install`
1. Запустить скрипты. (консольный php 7+ с установленными php-xml и php-mbstring)
