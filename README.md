# A set of utilities for tracking text-based game releases
Originally a parser for Russian Interactive Fiction, now it's much more than that.

[![pipeline status](https://gitlab.com/ifiction/news-script/badges/master/pipeline.svg)](https://gitlab.com/ifiction/news-script/commits/master)

[Русская версия readme](README.rus.md)

These are executable PHP scripts that serve different functions but share the code.

### run.php
This one was the first.
Its function is simple: it scans some game hosting sites, finds the new games (published during the last week) and prints a neat list in Markdown or HTML format.
All automatic.

### bot.php
This one does the same in continuous fashion, posting the feed to Mastodon or Telegram.

### wiki.php
And this one makes wiki pages for the [IFWiki](http://ifwiki.ru).
It uses Russian IFWiki syntax and templates but it would be easy to alter for the English IFWiki as well if you'd want that.

### kril.php
Just ignore this, it's for Russian IF comp.

## Installation

1. Install `pandoc` and `php` with `mbstring` and `xml` extensions.
1. Install [`composer`](https://getcomposer.org/) and execute `composer install`
1. Copy the `config.yml.example` to `config.yml`, edit it.
1. Run the scripts.
