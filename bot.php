#!/usr/bin/php
<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require "vendor/autoload.php";
use Symfony\Component\Yaml\Yaml;
use Oreolek\Game;
use Oreolek\Source;
use Oreolek\Publisher;
use Revolution\Mastodon\MastodonClient;
use \GuzzleHttp\Client as GuzzleClient;
use Mremi\UrlShortener\Provider\Bitly\BitlyProvider;
use Mremi\UrlShortener\Provider\Bitly\GenericAccessTokenAuthenticator;

$config = Yaml::parse(file_get_contents('config.yml'));
define('STYLE',$config['STYLE']);
define('FORMAT',$config['FORMAT']);

$mastodon = new Publisher\Mastodon($config);
$jabber = new Publisher\Jabber($config);
$telegram = new Publisher\Telegram($config);

// $jabber->publish('hello world!'); die();

$parsers = [
  // 'urq',
  'anivisual',
  'kvester',
  'apero',
  'instead',
  'hyperbook_ru',
  'hyperbook_en',
  'questbook',
  'textadventures',
  'ifdb',
  'dashingdon',
  'itch',
  'gamejolt',
  'steam',
];

$lastrun = 0;
if (file_exists('.lastrun') && !$config['DRY_RUN']) {
  $lastrun = file_get_contents('.lastrun');
}

$pandoc = new \Pandoc\Pandoc();

function check($classname, $command) {
  global $parsers;
  global $mastodon;
  global $jabber;
  global $telegram;
  global $config;
  if (in_array($command, $parsers)) {
    $cname = 'Oreolek\\Source\\'.$classname;
    $source = (new $cname());
    $source->check();
    if (empty($source->games)) {
      return;
    }
    foreach ($source->games as $game) {
      $description = $game->text();
      if ($config['DRY_RUN']) {
        echo $description."\n";
        continue;
      }
      $telegram->publish($description);
      $mastodon->publish($description);
      $jabber->publish($description);
    }
  }
}

foreach($config['parsers'] as $parser) {
  $command = strtolower($parser['classname']);
  if (isset($parser['command'])) {
    $command = $parser['command'];
  }
  check ($parser['classname'], $command);
}
