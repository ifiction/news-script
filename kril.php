#!/usr/bin/php
<?php
/*
 * Скрипт для того, чтобы заводить страницы русской IFWiki играм КРИЛа
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
use Symfony\Component\Yaml\Yaml;
use \GuzzleHttp\Client as GuzzleClient;

require "vendor/autoload.php";
require "Game.php";
require "Source.php";
require "Wikipage.php";
$config = Yaml::parse(file_get_contents('config.yml'));
$pandoc = new Pandoc\Pandoc();
$loader = new \Aura\Autoload\Loader;
$loader->register();
$loader->addPrefix('Source', 'Source');
$client = new GuzzleClient([
  'timeout' => 30,
]);

$api = json_decode(file_get_contents('http://forum.ifiction.ru/extern.php?action=kril2018'));
foreach ($api as $category) {
  $category_title = $category->category;
  $games = $category->games;
  foreach ($games as $game_data) {
    $game = new Game;
    $game->title = $game_data->full_title;
    $game->url_discussion = $game_data->url;
    $game->language = 'русский';
    $game->themes = [$game_data->rating];
    $game->description = $pandoc->runWith($game_data->description, [
      'from' => 'html',
      'to' => 'mediawiki'
    ]);
    if (isset($game_data->posters[0])) {
      $image_url = $game_data->posters[0];
      $response = $client->request('GET', $image_url, [
        'sink' => 'tempfile.jpg',
      ]);
      $filename = $response->getHeaderLine('Content-Disposition');
      preg_match('/\.(\w+)";\ssize/', $filename, $matches);
      if (isset($matches[1])) {
        $game->image_extension = $matches[1];
      }
      $image = file_get_contents('tempfile.jpg');
      $game->image_data = $image;
      unlink('tempfile.jpg');
    }
    $game->date = new \DateTime('08.12.2018');
    $game->author = $game_data->authors;
    $game->platform = $game_data->platform;
    $files = $game_data->files;
    if (isset($files[0])) {
      $game->url_download = $files[0]->url;
      $game->url_download_description = $files[0]->title;
    }
    if (isset($files[1])) {
      $game->url_online = $files[1]->url;
      $game->url_online_description = $files[1]->title;
    }
    $page = new Wikipage($game);
    $page->create();
  }
}
