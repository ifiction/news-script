#!/usr/bin/php
<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require "vendor/autoload.php";
use Symfony\Component\Yaml\Yaml;
use Oreolek\Game;
use Oreolek\Source;
use Oreolek\Wikipage;

$config = Yaml::parse(file_get_contents('config.yml'));
if (!isset($argv[1])) {
  echo 'Please provide the direct URL to the game page.'.PHP_EOL;
  die();
}
$url = $argv[1];
$game = new Game();

function check($classname) {
  global $game;
  global $url;

  $cname = 'Oreolek\\Source\\'.$classname;
  $cl = (new $cname());
  if ($cl->checkPage($url)) {
    echo 'Используется правило "'.$cl->title.'".'.PHP_EOL;
    if (!$cl->delayedLoad) {
      $cl->loadStr($cl->get_text($url));
    }
    $game = $cl->page($url);
    if ($game) {
      $page = new Wikipage($game);
      $page->create();
    }
  }
}

check ('Urq');
check ('Qsp');
check ('Kvester');
check ('Apero');
check ('Instead');
check ('Hyperbook');
check ('HyperbookEn');
check ('Questbook');
check ('Anivisual');
check ('Itch');
check ('Gamejolt');
check ('Steam');
