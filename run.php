#!/usr/bin/php
<?php
/*
  A set of utilities for tracking text-based game releases
  Copyright (C) 2017-2018  Alexander Yakovlev

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require "vendor/autoload.php";
use Symfony\Component\Yaml\Yaml;
use Oreolek\Game;
use Oreolek\Source;

$config = Yaml::parse(file_get_contents('config.yml'));
define('STYLE',$config['STYLE']);
define('FORMAT',$config['FORMAT']);

$parsers = 'all';
if (PHP_SAPI !== 'cli') {
  ob_start();
  echo '<!DOCTYPE html><html><body><code><pre>';
  if (isset($_GET['parsers'])) {
    $parsers = $_GET['parsers'];
    $parsers = explode(',', $parsers);
  }
} else {
  if (isset($argv[1])) {
    $parsers = explode(',', strtolower($argv[1]));
  }
}
if ($parsers === 'all') {
  $parsers = [
    'urq',
    'anivisual',
    'kvester',
    //'vndb',
    'apero',
    'instory',
    // 'instead',
    'hyperbook_ru',
    'hyperbook_en',
    'questbook',
    'textadventures',
    'ifdb',
    'dashingdon',
    'itch',
    'gamejolt',
    'steam',
  ];
}

function check($classname, $command) {
  global $parsers;
  if (is_array($parsers) && in_array($command, $parsers)) {
    $cname = 'Oreolek\\Source\\'.$classname;
    (new $cname())->check();
  }
}

check ('Urq', 'urq');
check ('Qsp', 'qsp');
check ('Kvester', 'kvester');
check ('Apero', 'apero');
check ('Instead', 'instead');
check ('Hyperbook', 'hyperbook_ru');
check ('Questbook', 'questbook');
//check ('Instory', 'instory');
check ('Anivisual', 'anivisual');
// ------- English online libraries
check ('HyperbookEn', 'hyperbook_en');
check ('Textadventures', 'textadventures');
check ('IFDB', 'ifdb');
check ('VNDB', 'vndb');
//check ('Dashingdon', 'dashingdon');
check ('Itch', 'itch');
check ('Gamejolt', 'gamejolt');
check ('Steam', 'steam');

if (PHP_SAPI !== 'cli') {
  echo '</pre></code></body></html>';
  ob_end_flush();
}
