<?php
/*
 * A set of utilities for tracking text-based game releases
 * Copyright (C) 2018  Alexander Yakovlev <keloero@oreolek.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'BaseTest.php';
use Oreolek\Source\Qsp;

class QspTest extends BaseTest
{
  protected function setUp(): void {
    $this->parser = new Qsp();
  }
  public function testPageCheck()
  {
    $this->assertTrue($this->parser->checkPage('http://qsp.su/index.php?option=com_sobi2&sobi2Task=sobi2Details&sobi2Id=364&Itemid=55'));
  }
  public function testPageParsing()
  {
    // we trick parser to think the page is online but we parse an offline copy
    $url = 'http://qsp.su/index.php?option=com_sobi2&sobi2Task=sobi2Details&sobi2Id=364&Itemid=55';
    $game_page = file_get_contents('./tests/fixtures/qsp_test_1.html');
    $this->parser->loadStr($game_page, []);
    $game = $this->parser->page($url);
    $this->assertSame($game->url, $url);
    $this->assertSame($game->title, 'Асатама II: Лунные земли');
    $this->assertSame($game->author, 'Mkir');
    $this->assertSame($game->platform, 'QSP');
  }
}
