<?php
/*
 * A set of utilities for tracking text-based game releases
 * Copyright (C) 2018  Alexander Yakovlev <keloero@oreolek.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'BaseTest.php';
use Oreolek\Source\Anivisual;

class AnivisualTest extends BaseTest
{
  protected function setUp(): void {
    $this->parser = new Anivisual();
  }
  public function testPageCheck()
  {
    $this->assertTrue($this->parser->checkPage('https://anivisual.net/stuff/3-1-0-1889'));
    $this->assertFalse($this->parser->checkPage('https://anivisual.net/'));
    $this->assertFalse($this->parser->checkPage('http://anivisual.net'));
    $this->assertFalse($this->parser->checkPage('http://itch.io'));
  }
  public function testPageParsing()
  {
    $url = 'https://anivisual.net/stuff/3-1-0-1889';
    $game_page = NULL;
    try {
      $game_page = $this->parser->get_text($url);
    } catch (\Exception $e) {
      $game_page = file_get_contents('./tests/fixtures/anivisual_test_1.html');
    }
    if (is_null($game_page)) {
      $game_page = file_get_contents('./tests/fixtures/anivisual_test_1.html');
    }
    $this->parser->loadStr($game_page, []);
    $game = $this->parser->page($url);
    $this->assertSame($url, $game->url);
    $this->assertSame('Contract Demon', $game->title);
    $this->assertSame('NomnomNami, пер. Sol Taere | Project Gardares', $game->author);
    $description = <<<END
Ангел, вместо своих ангельских дел, вызывает... демона. Да и демонесса какая-то странная, пытается выглядеть зловеще, но что-то совсем не получается. По крайней мере Ангел её не боится ни капли, а вроде даже наоборот...  _______ От переводчика.  Как я люблю эту короткометражную няшноту от NomnomNami, просто ужос.  Хотел было и дальше переводить её поделки, но узнал, что уже всё переведено. А жаль.  Работать с такими штуками - одно удовольствие. Лёгкий и приятный текст, и всего один-джва джня работы.  Прохождение этой ВН от Тернокса и Химари — р-р-раз, д-д-два    В новелле есть возможность переключения между английской версией и двумя версиями перевода Sol Taere и Project Gardares.
END;
    $this->assertSame($description, $game->description);
  }
}
