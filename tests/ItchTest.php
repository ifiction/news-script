<?php
/*
 * A set of utilities for tracking text-based game releases
 * Copyright (C) 2018  Alexander Yakovlev <keloero@oreolek.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'BaseTest.php';
use Oreolek\Source\Itch;

class ItchTest extends BaseTest
{
  protected function setUp(): void {
    $this->parser = new Itch();
  }
  public function testPageCheck()
  {
    $this->assertTrue($this->parser->checkPage('https://aetherinteractive.itch.io/subserial-network'));
    $this->assertTrue($this->parser->checkPage('http://zarf.itch.io/spider-and-web'));
    $this->assertFalse($this->parser->checkPage('https://itch.io'));
    $this->assertFalse($this->parser->checkPage('http://itch.io'));
  }
  public function testPageParsing()
  {
    // we trick parser to think the page is online but we parse an offline copy
    $url = 'https://zarf.itch.io/spider-and-web';
    $game_page = file_get_contents('./tests/fixtures/itch_test_1.html');
    $this->parser->loadStr($game_page, []);
    $game = $this->parser->page($url);
    $this->assertSame($url, $game->url);
    $this->assertSame('Spider And Web', $game->title);
    $this->assertSame('zarf', $game->author);
    $this->assertSame(NULL, $game->description);
    $this->assertSame('Interactive fiction', $game->short_description);
  }
}
